/* 
 * File:   ui.h
 * Author: rfrost
 *
 * Created on May 3, 2021, 10:31 AM
 */

#ifndef UI_H
#define	UI_H

#include <xc.h>

// system operating / LCD sequence states
#define OFF 0
#define COOLING 1
#define HEATING 2
#define INITIALIZING 2

// button press states
#define NONE 0 
#define ONCE 1
#define TWICE 2

// UI timer definitions (timer 3)
#define UI_TIMER_ON T3CONbits.TON = 1
#define UI_TIMER_OFF T3CONbits.TON = 0
#define UI_TIMER_RESET TMR3 = 0
#define UI_TIMER_PERIOD PR3 = 0x4379; // ~150 ms, PRE = 64;
#define UI_TIMER_CONFIG T3CONbits.TCS = 0; T3CONbits.TGATE = 0; T3CONbits.TCKPS = 0b10;
#define UI_TIMER_INTERRUPT_CONFIG IPC2bits.T3IP = 1; IFS0bits.T3IF = 0; IEC0bits.T3IE = 1;
#define UI_TIMER_FLAG_CLEAR IFS0bits.T3IF = 0

void configure_ui(void);

#endif	/* UI_H */

