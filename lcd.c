#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xc.h>

#include "buttons.h"
#include "DS1631.h"
#include "fonts.h"
#include "i2c.h"
#include "interrupts.h"
#include "lcd.h"
#include "rgbLED.h"
#include "spi.h"
#include "uart.h"
#include "ui.h"

void LCDWrite(LCDBYTE dc, LCDBYTE data) {
    LCD_SCE = LOW;    // Selection LCD panel
    LCD_DC = dc;      // Indicate Data/Command signal
    SPI1BUF = data;   // Put data in output buffer for SPI1
    while(!SPI1STATbits.SPIRBF){} // wait for it to send
    Nop();            // A little extra wait time
    LCD_SCE = HIGH;   // Deselect LCD panel
}

void LCDCharacter(char character) {
    int index;
    LCDWrite(LCD_DATA, 0x00);
    for (index = 0; index < 5; index++) {
        LCDWrite(LCD_DATA, ASCII[character - 0x20][index]);
    }
    LCDWrite(LCD_DATA, 0x00);
}

void LCDString(char string[]) {
    int length = strlen(string);
    int i;
    for(i=0;i<length;i++){
        LCDCharacter(string[i]);
    }
}

void LCDclr(void) {
    int i;
    for (i=0;i<6*84;i++){
        LCDWrite(LCD_DATA,0x00);
    }
    LCDWrite(LCD_COMMAND,0x40);
    LCDWrite(LCD_COMMAND,0x80);
}

void configure_lcd(void) {
    LCD_SCE_PIN = OUT;
    LCD_RESET_PIN = OUT;
    LCD_DC_PIN = OUT;
    
    LCD_RESET = LOW;
    LCD_RESET = HIGH;
    
    LCDWrite(LCD_COMMAND, 0x21);   // LCD Extended Commands.
    LCDWrite(LCD_COMMAND, 0x20);  // Set LCD Vop (Contrast). 
    LCDWrite(LCD_COMMAND, 0x04);  // Set Temp coefficent. //0x04
    LCDWrite(LCD_COMMAND, 0x13);  // LCD bias mode 1:48. //0x13
    LCDWrite(LCD_COMMAND, 0x20);  // LCD Basic Commands
    LCDWrite(LCD_COMMAND, 0x0C);  // LCD in normal mode.
}