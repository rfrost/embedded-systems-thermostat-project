/* 
 * File:   main.c
 * Author: rfrost
 *
 * Created on May 3, 2021, 10:32 AM
 */

#include <xc.h>
#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "buttons.h"
#include "DS1631.h"
#include "fonts.h"
#include "i2c.h"
#include "interrupts.h"
#include "lcd.h"
#include "rgbLED.h"
#include "spi.h"
#include "uart.h"
#include "ui.h"

extern int on_off_state;

int main(void) {
    
    // setup serial logging
    config_UART();
    printf("\033[2J\033[;H");
    printf("BEGINNING OF LOG\r\n\n");
    printf("Initializing System\r\n");
    
    printf("    Configuring Hardware\r\n");
    // setup LCD screen to provide user messages
    configure_spi();
    configure_interrupts();
    configure_lcd();
    LCDclr();
    LCDString("Initializing");
    LCDString("            ");
    LCDString("Push buttons");
    LCDString("to start...  ");
    while(on_off_state == INITIALIZING);
    // configure system
    config_I2C2();
    config_DS1631();
    configure_pushbuttons();
    configure_rgbLED();
    
    printf("    Start Measuring Temperature\r\n");
    DS1631_startConvert();
    
    printf("    Start the UI\r\n");
    configure_ui();
    
    printf("System Up and Running\r\n");
    
    while(1) {
        Idle();
    }
            
    return (0);
}
