/* 
 * File:   i2c.h
 * Author: rfrost
 *
 * Created on May 3, 2021, 10:26 AM
 */

#ifndef I2C_H
#define	I2C_H

#include <xc.h>

#define I2C_ACK 0
#define I2C_NACK 1

typedef unsigned char BYTE;

void config_I2C2(void);
void startI2C2(void);
void restartI2C2(void);
BYTE putI2C2(BYTE);
void stopI2C2(void);
BYTE getI2C2(BYTE);

#endif	/* I2C_H */


