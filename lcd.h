/* 
 * File:   lcd.h
 * Author: rfrost
 *
 * Created on May 3, 2021, 10:30 AM
 */

#ifndef LCD_H
#define	LCD_H

#include <xc.h>

typedef unsigned char LCDBYTE;

// Pins for LCD Control Signals
#define LCD_SCE LATBbits.LATB12
#define LCD_SCE_PIN TRISBbits.TRISB12
#define LCD_RESET LATBbits.LATB13
#define LCD_RESET_PIN TRISBbits.TRISB13
#define LCD_DC LATBbits.LATB14
#define LCD_DC_PIN TRISBbits.TRISB14

#define IN 1
#define OUT 0

#define LOW 0
#define HIGH 1

#define LCD_COMMAND  LOW
#define LCD_DATA     HIGH

void LCDWrite(LCDBYTE, LCDBYTE);
void LCDCharacter(char c);
void LCDString(char string[]);
void LCDclr(void);
void configure_LCD(void);

#endif	/* LCD_H */

