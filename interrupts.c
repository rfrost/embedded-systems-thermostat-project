#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xc.h>

#include "buttons.h"
#include "DS1631.h"
#include "fonts.h"
#include "i2c.h"
#include "interrupts.h"
#include "lcd.h"
#include "rgbLED.h"
#include "spi.h"
#include "uart.h"
#include "ui.h"

/* global variables from ui.c */
// state of the system
extern int system_State;
extern int old_system_State;

// tracking on/off
extern int on_off_state;

// operating mode
extern int programming_State;
extern int old_programming_State;

// tracking temperatures
extern float temp;
extern float old_temp;
extern float low_temp;
extern float old_low_temp;
extern float high_temp;
extern float old_high_temp;

// LCD screen sequence state
extern int screen_Sequence;

// tracking button pushes
extern int buttons_State;

/* global variables from rgbLED.c*/
// Keeps track of the current operation mode of the LED
extern int rgbLED_State;           // ON, OFF, DIM
// Keeps track of the instant value of the LED
extern int rgbLED_Value;           // ON, OFF
// Keeps track of the color of the LED when on
extern int rgbLED_Color;       // RED, MAGENTA, BLUE, CYAN, GREEN, YELLOW, WHITE
// Keeps track of the current dimmer setting
extern int rgbLED_dimValue;        // 0-100, 0 off, 100 on

// variables for printing to LCD
char tempchar[];
char statechar;
char unused[20];
char low_temp_char[];
char unused2[20];
char high_temp_char[];

// variables for controlling prints to serial monitor
int programming_flag;
int low_temp_flag;
int high_temp_flag;
int exit_programming_flag;

/* INTERRUPTS */
void __attribute__((__interrupt__, no_auto_psv)) _T1Interrupt(void)
{
    // Controls the generation of the PWM waveform based on value of dimValue
    PWM_TIMER_ON;
    if(rgbLED_Value == OFF) {
        rgbLED_on();
        PR1 = (rgbLED_dimValue)*(PWM_TIMER_CYCLE_TIME/100);
    } else {
        rgbLED_off();
        PR1 = (100-rgbLED_dimValue)*(PWM_TIMER_CYCLE_TIME/100);
    } 

    PWM_TIMER_FLAG_CLEAR;   // Clear Timer1 Interrupt flag
}

void __attribute__((__interrupt__,no_auto_psv)) _CNInterrupt(void) {
    IEC1bits.CNIE = 0;      // disable CN interrupt
 
    IFS1bits.CNIF = 0;      // clear CN interrupt flag
    IFS0bits.T2IF = 0;      // clear timer 2 interrupt flag
    
    T2CONbits.TCKPS = 0b01; // return to 1:1 pre-scale
    PR2 = 0x0715;           // configure interrupt time
    
    T2CONbits.TON = 1;      // turn on timer 2
}

void __attribute__((__interrupt__,no_auto_psv)) _T2Interrupt(void) {
    T2CONbits.TON = 0;      // turn off timer 2
    IFS0bits.T2IF = 0;      // clear timer 2 interrupt flag
    
    // if both buttons pressed and system just started up, turn it on
    if ((LEFT_PUSH == PRESSED) && (RIGHT_PUSH == PRESSED) && (buttons_State == NONE) && (on_off_state == INITIALIZING)) {
        on_off_state = ON;
    }
    // if both buttons pressed for "first" time, enter programming mode and start
    // by setting the low temp set point
    else if ((LEFT_PUSH == PRESSED) && (RIGHT_PUSH == PRESSED) && (buttons_State == NONE) && (on_off_state == ON)) {
        programming_State = ON;
        programming_flag = ON;
        buttons_State = ONCE;
        low_temp_flag = ON;
    }
    // if programming mode is entered, and right button is pressed, it either
    // increases the set point of the low or high temp, depending on which is 
    // currently being programmed
    else if ((RIGHT_PUSH == PRESSED) && (LEFT_PUSH == RELEASED) && (programming_State == ON) && (on_off_state == ON)) {
        if(buttons_State == ONCE) {
            old_low_temp = low_temp;
            
            // guard logic so low set point must be lower than high set point
            if (low_temp < (high_temp - 0.2)) {
                low_temp = low_temp + 0.1;
            }
            else {
                low_temp = low_temp;
            }
        }
        else if (buttons_State == TWICE) {
            old_high_temp = high_temp;
            high_temp = high_temp + 0.1;
        }
        else {
            old_low_temp = low_temp;
            old_high_temp = high_temp;
        }
        
        T2CONbits.TCKPS = 0b01; // 1:8 pre-scale for second delay
        PR2 = 0xFFFF;           // reasonable checking time for button hold
        T2CONbits.TON = 1;      // turn on timer 2
    }
    // if programming mode is entered, and left button is pressed, it either
    // decreases the set point of the low or high temp, depending on which is 
    // currently being programmed
    else if ((RIGHT_PUSH == RELEASED) && (LEFT_PUSH == PRESSED) && (programming_State == ON) && (on_off_state == ON)) {
        if (buttons_State == ONCE) {
            old_low_temp = low_temp;
            low_temp = low_temp - 0.1;
        }
        else if (buttons_State == TWICE) {
            old_high_temp = high_temp;
            // guard logic so high set point must be higher than low set point
            if (high_temp > (low_temp + 0.2)) {
                high_temp = high_temp - 0.1;
            }
            else {
                high_temp = high_temp;
            }
        }
        else {
            old_low_temp = low_temp;
            old_high_temp = high_temp;
        }
        
        T2CONbits.TCKPS = 0b01; // 1:8 pre-scale for second delay
        PR2 = 0xFFFF;           // reasonable checking time for button hold
        T2CONbits.TON = 1;      // turn on timer 2
    }
    // if both buttons pressed for a second time, it changes to set the high 
    // temp set point
    else if ((LEFT_PUSH == PRESSED) && (RIGHT_PUSH == PRESSED) && (buttons_State == ONCE) && (on_off_state == ON)) {
        buttons_State = TWICE;
        high_temp_flag = ON;
    }
    else if ((LEFT_PUSH == PRESSED) && (RIGHT_PUSH == PRESSED) && (buttons_State == TWICE) && (on_off_state == ON)) {
        programming_State = OFF;
        buttons_State = NONE;
        exit_programming_flag = ON;
    }
    else {
        old_low_temp = low_temp;
        old_high_temp = high_temp;
    }
    
    IFS1bits.CNIF = 0;      // turn off CN interrupt flag
    IEC1bits.CNIE = 1;      // enable CN interrupt
}

void __attribute__((__interrupt__,no_auto_psv)) _T3Interrupt(void) {
    // clear LCD screen
    LCDclr();
    
    // read temperature
    old_temp = temp;
    temp = DS1631_readTemp();
    check_Temp();
    
    // convert temp and temp set points to 1 decimal character strings for 
    // displaying to LCD
    sprintf(tempchar, "%4.1f", temp);
    sprintf(low_temp_char, "%4.1f", low_temp);
    sprintf(high_temp_char, "%4.1f", high_temp);
    
    // controls writing to LCD screen
    if (programming_State == OFF) {
        LCDString("Temp: ");
        LCDString(tempchar);
        LCDString("  ");
        LCDString("            ");
        if (system_State == COOLING) {
            LCDString("Cooling...");
        }
        else if (system_State == HEATING) {
            LCDString("Heating...");
        }
        else {
            LCDString("System Off");
        }
    }
    else if (programming_State == ON) {
        LCDString("Temp: ");
        LCDString(tempchar);
        LCDString("              ");
        LCDString("[");
        LCDString(low_temp_char);
        LCDString(",");
        LCDString(high_temp_char);
        LCDString("]");
        
        if (buttons_State == ONCE) {
            LCDString("     ^       ");
        }
        else if (buttons_State == TWICE) {
            LCDString("          ^  ");
        }
        
        if (system_State == COOLING) {
            LCDString("Cooling...");
        }
        else if (system_State == HEATING) {
            LCDString("Heating...");
        }
        else {
            LCDString("System Off.");
        }
    }
    
    // controls writing to serial monitor output
    if ((temp > (old_temp + 0.1)) || (temp < (old_temp - 0.1))) {
        printf("    Temperature changed to %4.1f F from %4.1f F.\r\n", temp, old_temp);
    }
    
    if (low_temp != old_low_temp) {
        printf("    Low temperature set point changed to %4.1f F.\r\n", low_temp);
    }
    
    if (high_temp != old_high_temp) {
        printf("    High temperature set point changed to %4.1f F.\r\n", high_temp);
    }
    
    if (system_State != old_system_State) {
        if ((old_system_State == OFF) && (system_State == COOLING)) {
            printf("    System is cooling...\r\n");
        }
        else if ((old_system_State == OFF) && (system_State == HEATING)) {
            printf("    System is heating...\r\n");
        }
        else if ((old_system_State == COOLING) && (system_State == OFF)) {
            printf("    Temperature within specified range, system is off.\r\n");
        }
        else if ((old_system_State == COOLING) && (system_State == HEATING)) {
            printf("    System switched from cooling to heating.\r\n");
        }
        else if ((old_system_State == HEATING) && (system_State == OFF)) {
            printf("    Temperature within specified range, system is off.\r\n");
        }
        else if ((old_system_State == HEATING) && (system_State == COOLING)) {
            printf("    System switched from heating to cooling.\r\n");
        }
    }
    
    if ((programming_State == ON) && (programming_flag == ON)) {
        printf("    Programming mode enabled.\r\n");
        programming_flag = OFF;
    }
    if ((programming_State == ON) && (buttons_State == ONCE) && (low_temp_flag == ON)) {
        printf("    Setting low temp point...\r\n");
        low_temp_flag = OFF;
    }          
    if ((programming_State == ON) && (buttons_State == TWICE) && (high_temp_flag == ON)) {
        printf("    Setting high temp point...\r\n");
        high_temp_flag = OFF;
    }
    if ((programming_State == OFF) && (exit_programming_flag == ON)) {
        printf("    Programming mode exited.\r\n");
        exit_programming_flag = OFF;
    }
    
    // turn on LED
    rgbLED_Set(rgbLED_Color, rgbLED_dimValue);
    
    // clear T3 flag
    UI_TIMER_FLAG_CLEAR;
}

// function for setting LED and system state based on temp
void check_Temp(){
    rgbLED_dimValue = 100;
    
    if (temp >= high_temp) {
        rgbLED_Color = BLUE;
        old_system_State = system_State;
        system_State = COOLING;
    }
    else if (temp <= low_temp) {
        rgbLED_Color = RED;
        old_system_State = system_State;
        system_State = HEATING;
    }
    else {
        rgbLED_Color = GREEN;
        old_system_State = system_State;
        system_State = OFF;
    }
}

// interrupt configuration functions
void initInterrupts(void) {
    INTCON2bits.GIE = 1;    // enable level 1-7 interrupts
    
    // change notification configuration
    IFS1bits.CNIF = 0;      // clear CN interrupt flag
    IPC4bits.CNIP = 4;      // set CN interrupt priority to 4
    IEC1bits.CNIE = 1;      // enable CN interrupts
    
    // timer 2 configuration
    T2CONbits.TON = 0;      // disable timer 2
    T2CONbits.TCS = 0;      // use internal clock (Fosc/2)
    T2CONbits.T32 = 0;      // 16-bit timer mode
    T2CONbits.TGATE = 0;    // disable clock gating
    T2CONbits.TCKPS = 0b00; // set a 1:1 pre-scale value
    TMR2 = 0;
    PR2 = 0x4807;           // configure interrupt at 5ms
    
    IPC1bits.T2IP = 3;      // set T2 priority level to 3
    IFS0bits.T2IF = 0;      // clear timer 2 interrupt flag
    IEC0bits.T2IE = 1;      // enable interrupt 
}

void configPins(void) {
    // push button wired to pin 9: RB3
    TRISAbits.TRISA2 = 1;   // set RA2 on input pin
    CNENAbits.CNIEA2 = 1;   // enable CN for pin RA2
    CNPUAbits.CNPUA2 = 1;   // enable CN pull-up for RA2
    
    // push button wired to pin 11: RB4
    TRISBbits.TRISB4 = 1;   // set RB4 on input pin
    CNENBbits.CNIEB4 = 1;   // enable CN for pin RB4
    CNPUBbits.CNPUB4 = 1;   // enable CN pull-up for RB4
}

void configure_interrupts(void) {
    initInterrupts();
    configPins();
}

