/* 
 * File:   DS1631.h
 * Author: rfrost
 *
 * Created on May 3, 2021, 10:26 AM
 */

#ifndef DS1631_H
#define	DS1631_H

#include <xc.h>

#define FCY 7370000/2   // clock frequency
#define FSCL 100        // SPI clock frequency in kHz

#define NO_I2C_ERROR 1
#define I2C_ERROR 0

#define DS1631_ADDRESS 0b10010000       // yes
#define DS1631_CMD_ACCESS_CONFIG 0xAC   // yes
#define DS1631_CMD_START_CONVERT 0x51   // yes
#define DS1631_CMD_STOP_CONVERT 0x22    // NO
#define DS1631_CMD_READ_TEMP 0xAA       // yes
#define DS1631_CMD_ACCESS_TH 0xA1       // NO
#define DS1631_CMD_ACCESS_TL 0xA2       // NO
#define DS1631_CMD_SOFTWARE_POR 0x54    // NO

void config_DS1631(void);
void DS1631_startConvert(void);
float DS1631_readTemp(void);
void check_Temp();

#endif	/* DS1631_H */

