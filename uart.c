#include <stdio.h>
#include <stdlib.h>
#include <xc.h>

#include "buttons.h"
#include "DS1631.h"
#include "fonts.h"
#include "i2c.h"
#include "interrupts.h"
#include "lcd.h"
#include "rgbLED.h"
#include "spi.h"
#include "uart.h"
#include "ui.h"


void config_UART(void) {
    RPINR18bits.U1RXR = 42;         // map UART1 RX to re-mappable pin RP42
    RPOR4bits.RP43R = 1;            // map UART1 TX to re-mappable pin RP43         

    int brg = (FCY/BAUDRATE/4)-1;   // calculate the Baud Rate generator value
    U1MODEbits.BRGH = 1;            // high speed mode
    U1BRG = brg;                    // store the value in the register
    
    U1MODEbits.PDSEL = 0;           // 8 bit data, no parity
    U1MODEbits.STSEL = 0;           // 1 stop bit
    
    U1MODEbits.UEN = 0b00;          // UxTX and UxRX pins are enabled and used; no flow control pins
    U1MODEbits.UARTEN = 1;          // enable UART RX/TX
    U1STAbits.UTXEN = 1;            // enable the transmitter
}

void putch(char c){
    while(U1STAbits.UTXBF);         // wait for transmit buffer to be empty
    U1TXREG = c;                    // write character to transmit register
}

int inChar(void) {
    while(!U1STAbits.URXDA);        // data receieved yet
    return(U1RXREG);                // return data in receieve buffer
}
