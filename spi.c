#include <stdio.h>
#include <stdlib.h>
#include <xc.h>

#include "buttons.h"
#include "DS1631.h"
#include "fonts.h"
#include "i2c.h"
#include "interrupts.h"
#include "lcd.h"
#include "rgbLED.h"
#include "spi.h"
#include "uart.h"
#include "ui.h"

void configure_spi(void) {
    SPI1CON1bits.MSTEN = 1;    // make master
    SPI1CON1bits.PPRE = 0b11;  // 1:1 primary prescale
    SPI1CON1bits.SPRE = 0b111; // 1:1 secondary prescale
    SPI1CON1bits.MODE16 = 0;   // 8-bit transfer mode
    SPI1CON1bits.SMP = 0;      // sample in middle
    SPI1CON1bits.CKE = 1;      // Output on falling edge
    SPI1CON1bits.CKP = 0;      // CLK idle state low
    SPI1STATbits.SPIEN = 1;    // enable SPI1
}