/* 
 * File:   buttons.h
 * Author: rfrost
 *
 * Created on May 3, 2021, 10:26 AM
 */

#ifndef BUTTONS_H
#define	BUTTONS_H

#include <xc.h>

// push buttons
#define LEFT_PUSH_PIN TRISAbits.TRISA2  // left push button on Port A2
#define LEFT_PUSH PORTAbits.RA2
#define RIGHT_PUSH_PIN TRISBbits.TRISB4 // right push button on Port B4
#define RIGHT_PUSH PORTBbits.RB4

// button states
#define PRESSED 0
#define RELEASED 1

void configure_pushbuttons(void);

#endif	/* BUTTONS_H */

