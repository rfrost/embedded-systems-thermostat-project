/* 
 * File:   uart.h
 * Author: rfrost
 *
 * Created on May 3, 2021, 10:26 AM
 */

#ifndef UART_H
#define	UART_H

#include <xc.h>

#define BAUDRATE 19200
#define FCY 7370000/2

void config_UART(void);
void putch(char c);
int inChar(void);

#endif	/* UART_H */

