#include <stdio.h>
#include <stdlib.h>
#include <xc.h>

#include "buttons.h"
#include "DS1631.h"
#include "fonts.h"
#include "i2c.h"
#include "interrupts.h"
#include "lcd.h"
#include "rgbLED.h"
#include "spi.h"
#include "uart.h"
#include "ui.h"

void configure_pushbuttons(void) {
    LEFT_PUSH_PIN = IN;
    RIGHT_PUSH_PIN = IN;
}
