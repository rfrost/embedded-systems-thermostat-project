#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

#include "buttons.h"
#include "DS1631.h"
#include "fonts.h"
#include "i2c.h"
#include "interrupts.h"
#include "lcd.h"
#include "rgbLED.h"
#include "spi.h"
#include "uart.h"
#include "ui.h"

// state of the system
int system_State = INITIALIZING;
int old_system_State = OFF;

// determine when system is on
int on_off_state = INITIALIZING;

// operating mode
int programming_State = OFF;
int old_programming_State = OFF;

// tracking temperatures
float temp;
float old_temp = 72.0;
float low_temp = 73.0;
float old_low_temp = 0;
float high_temp = 75.0;
float old_high_temp = 0;

// LCD screen sequence state
int screen_Sequence = ON;

// tracking button pushes
int buttons_State = NONE;

void configure_ui(void) {
    // configure UI timer
    UI_TIMER_OFF;
    UI_TIMER_CONFIG;
    UI_TIMER_INTERRUPT_CONFIG;
    UI_TIMER_RESET;
    UI_TIMER_PERIOD;
    UI_TIMER_ON;
}
