/* 
 * File:   interrupts.h
 * Author: rfrost
 *
 * Created on May 11, 2021, 11:03 AM
 */

#ifndef INTERRUPTS_H
#define	INTERRUPTS_H

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>

#include "buttons.h"
#include "DS1631.h"
#include "i2c.h"
#include "lcd.h"
#include "rgbLED.h"
#include "spi.h"
#include "trigger.h"
#include "uart.h"
#include "ui.h"

void checkTemp(void);
void initInterrupts(void);
void configPins(void);
void configure_interrupts(void);


#endif	/* INTERRUPTS_H */

