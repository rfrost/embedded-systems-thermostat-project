#include <stdio.h>
#include <stdlib.h>
#include <xc.h>

#include "buttons.h"
#include "DS1631.h"
#include "fonts.h"
#include "i2c.h"
#include "interrupts.h"
#include "lcd.h"
#include "rgbLED.h"
#include "spi.h"
#include "uart.h"
#include "ui.h"

BYTE temp_hi;
BYTE temp_lo;
float tempC;
float tempF;

void config_DS1631(){
    startI2C2();                         // start transfer
    putI2C2(DS1631_ADDRESS & 0xFE);      // send address, lsb 0
    putI2C2(DS1631_CMD_ACCESS_CONFIG);   // send access config cmd
    putI2C2(0x0C);                       // send configuration
    stopI2C2();                          // stop transfer
}

void DS1631_startConvert(){
    startI2C2();                          // start transfer
    putI2C2(DS1631_ADDRESS & 0xFE);       // send address, lsb 0
    putI2C2(DS1631_CMD_START_CONVERT);    // start converting
    stopI2C2();                           // stop transfer
}

float DS1631_readTemp(){
    startI2C2();                     // start transfer
    putI2C2(DS1631_ADDRESS & 0xFE);  // send address, lsb 0
    putI2C2(DS1631_CMD_READ_TEMP);   // send read temp cmd
    restartI2C2();                   // restart transfer
    putI2C2(DS1631_ADDRESS | 0x01);  // send address, lsb 1
    temp_hi = getI2C2(I2C_ACK);      // get MSB of temp
    temp_lo = getI2C2(I2C_NACK);     // get LSB of temp
    stopI2C2();
    
    tempC = (temp_hi) + ((float)temp_lo)/(256.0); // convert to C temp
    tempF = tempC*(1.8) + 32; // convert C to F
    
    return(tempF);
}
