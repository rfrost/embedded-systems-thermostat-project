/* 
 * File:   spi.h
 * Author: rfrost
 *
 * Created on May 3, 2021, 10:31 AM
 */

#ifndef SPI_H
#define	SPI_H

#include <xc.h>

// Pins for SPIx 
#define USESPI1 // define to USESPI1/USESPI2 depending on what set you are using

void configure_spi(void);

#endif	/* SPI_H */

